package jric.cleaner;

import java.io.File;
import java.io.IOException;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;

	/**
	 * @author jwb09119
	 * @date 2014/8/15
	 */

public class JavaParserWrapper {
	
	public JavaParserWrapper () {
		
	}

	public void findAndReplaceIdentifier(File file, String word, String replacement) throws ParseException, IOException {
		System.out.println("\tReplacing \""+word+"\" with \""+replacement+"\"  in: "+file.getName());
		
		CompilationUnit cu = JavaParser.parse(file);
	}

}
