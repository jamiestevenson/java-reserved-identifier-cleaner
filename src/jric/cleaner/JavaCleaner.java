package jric.cleaner;

import japa.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class JavaCleaner implements Cleaner {
	
	JavaParserWrapper jpw;
	
	// Constructor
	public JavaCleaner() {
		jpw = new JavaParserWrapper();
	}
	
	
	@Override
	public boolean cleanSourceFiles(File location) {
		
		Collection<File> files = getAllJavaFilesAtLocation(location);
		
		files.stream().forEach(file -> cleanFile(file));

		return true;
		
	}
	
	private void cleanFile(File file) {
		
		if (!fileIsClean(file)) {
		
			String replacement = getNovelReplacement(file, "enum");
			
			try {
				jpw.findAndReplaceIdentifier(file, "enum", replacement);
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}

	}

	
	private boolean fileIsClean(File file) {
		
		return !fileContainsWord(file, "enum");
		
	}

	
	private String getNovelReplacement (File file, String word) {
		
		String novelWord = addPrefix(word);
		
		if (fileContainsWord(file, novelWord)) {
			
			return getNovelReplacement(file, novelWord);
			
		} else {
			
			return novelWord;
			
		}
		
	}
	
	
	private String addPrefix (String word) {
		return '_'+word;
	}
	
	private boolean fileContainsWord(File file, String word) {
		
		boolean containsWord = false;
		
		try {
			
			List<String> lines = Files.readAllLines(file.toPath());
			containsWord = lines.stream().anyMatch(line -> line.contains(word));
			
		} catch (IOException e) {
			
			throw new RuntimeException("Can't access file: "+file.getAbsolutePath(), e);
		
		}
		
		return containsWord;
		
	}
	
	
	private Collection<File> getAllJavaFilesAtLocation (File location) {
		
		Collection<File> reply = new ArrayList<>();
		
		if (location.isFile() && location.getName().endsWith(".java")) {
			reply.add(location);
			return reply;
		}
		
		Stream<File> inDirectory = Arrays.asList(location.listFiles()).stream();
		
		Consumer<File> recursiveCollect = (file) -> {
				if (file.isDirectory()) {
					getAllJavaFilesAtLocation(file);
				} else if (file.isFile() && file.getName().endsWith(".java")) {
					reply.add(file);
				}
			};
		
		inDirectory.forEach(recursiveCollect);
			
		return reply;
		
	}

}
