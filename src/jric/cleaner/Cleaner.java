package jric.cleaner;

import java.io.File;

	/**
	 * @author jwb09119
	 * @date 2014/8/15
	 */

public interface Cleaner {

	/**
	 * Cleans all source files in this location.
	 * @param location - target file or directory
	 * @return - true if clean was successful, otherwise false
	 */
	public boolean cleanSourceFiles(File location);
	
}
