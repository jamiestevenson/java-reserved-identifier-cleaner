package jric;

import java.io.File;

import jric.cleaner.Cleaner;
import jric.cleaner.JavaCleaner;

	/**
	 * @author jwb09119
	 * @date 2014/8/15
	 * 
	 * Java Reserved Identifier Cleaner
	 * 
	 * Initial goal is to use this as a library dependency, so there will be no properly documented
	 * jar runnable for now.
	 */

class RunThis {
	
	final static String VERSION = "v0.1";
	
	public static void main (String... args) {
		
		System.out.println("J.R.I.C. - Java Reserved Identifier Cleaner "+VERSION);
		
		if (args.length < 1) {
			throw new RuntimeException("No arguments! - provide a file or directory location");
		}
		
		File targetLocation = new File(args[0]);
		
		if (targetLocation.exists()) {
			System.out.println("Target: "+targetLocation.getAbsolutePath());
		} else {
			throw new RuntimeException("Can't access file: "+targetLocation.getAbsolutePath());
		}
		
		Cleaner c = new JavaCleaner();
		c.cleanSourceFiles(targetLocation);
		
	}

}
