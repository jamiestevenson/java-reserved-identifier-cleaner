package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.cleaner.JavaCleanerTest;

@RunWith(Suite.class)
@SuiteClasses({JavaCleanerTest.class})
public class AllTests {

}
